from datetime import datetime

# Define the main function
def main():
    print("Hello World!") #print stuff in brackets
    print(f"Current time is {datetime.now().strftime('%H:%M:%S')}") #print current time


if __name__ == "__main__":
    main()
